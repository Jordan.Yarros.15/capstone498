<?php
// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Check if the username is assigned
if(!isset($_SESSION["username"])){
    header("location: login.php");
    exit;
} else {
	$identity = trim($_SESSION["username"]);
}



/* Database credentials. Assuming you are running MySQL
server with default setting (user 'root' with no password) */
define('RDS_HOSTNAME', 'aa1u4nfojyef6ry.cpwluseulq90.us-west-2.rds.amazonaws.com');
define('RDS_USERNAME', 'JordanCapstone');
/* Secure password */
define('RDS_PASSWORD', '99109445Jy!');
define('RDS_DB_NAME', 'aa1u4nfojyef6ry');
define('RDS_Port', '3306');

/* Attempt to connect to MySQL database */
$connect = mysqli_connect($_SERVER['RDS_HOSTNAME'], $_SERVER['RDS_USERNAME'], $_SERVER['RDS_PASSWORD'], $_SERVER['RDS_DB_NAME'], $_SERVER['RDS_PORT']);

// Check connection
if($connect === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}

if(isset($_GET['value_key'])){
  $var = trim($_GET['value_key']); //some_value
}
if(isset($_GET['col'])){
  $varTwo = trim($_GET['col']); //some_value
}

// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){



  // getting post
    if(empty(trim($_POST["stat"]))){
        $stat_err = "Please enter new stat";
    } else{
        $stat = trim($_POST["stat"]);
    }
    
     // getting post
    if(empty(trim($_POST["statID"]))){
        $statID_err = "Please enter player id";
    } else{
        $var = trim($_POST["statID"]);
    }
    
     // getting post
    if(empty(trim($_POST["statType"]))){
        $statType_err = "Please enter new stat";
    } else{
        $varTwo = trim($_POST["statType"]);
    }
	
 		$update = "update playersWeeklyStats set ".$varTwo." = ".$stat." where id = ".$var.";";																		
 		$result = mysqli_prepare($connect, $update);
         if( mysqli_stmt_execute($result)){
         
         	header("location: http://premierplaza-env-1.2qvmgmhmmu.us-west-2.elasticbeanstalk.com/x_fresh_bootstrap_table_v1.1/stats.php");
  				exit;
         }											
 
 
 // Close connection
mysqli_close($link);
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
         .wrap{ 
        font: 14px sans-serif; 
        display: -webkit-box;
  		display: flex;
  		-ms-flex-align: center;
  		-ms-flex-pack: center;
  		-webkit-box-align: center;
  		align-items: center;
  		-webkit-box-pack: center;
  		justify-content: center;
  		padding-top: 40px;
  		padding-bottom: 40px;
  		background-color: #f5f5f5;
        }
        .wrapper{ width: 350px; padding: 20px; }
        .navbar{padding-right: 20px;}
    </style>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-default navbar-inverse bg-light">
    <ul class="nav navbar-nav">
    <li>
        <a class="nav-link" href="welcome.php">Home </a>
      </li>
      <li>
        <a class="nav-link" href="joinLeague.php">Join League </a>
      </li>
      <li>
      <a class="nav-link" href="createLeague.php">Create League </a>
      </li>
      <li>
        <a class="nav-link" href="/x_fresh_bootstrap_table_v1.1/full-screen-table.php">Browse Players</a>
      </li>
       <li>
        <a class="nav-link" href="addNewPlayer.php">Add New Player</a>
      </li>
      <li>
        <a class="nav-link" href="/x_fresh_bootstrap_table_v1.1/stats.php">Game Day Stats</a>
      </li>
      </ul>
    <span>
    <ul class="nav navbar-nav navbar-right">
      <li>
        <a class="nav-link" href="reset-password.php">Reset Password</a>
      </li>
      <li>
     	<a class="nav-link" href="logout.php">Logout</a>
      </li>
    </ul>
    </span>
  </div>
</nav>

<div class = wrap>
<div class="wrapper">
        <h2>Edit Player Stat</h2>
        <p>Type in the correct statistic</p>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group <?php echo (!empty($stat_err)) ? 'has-error' : ''; ?>">
                <label>Corrected Stat</label>
                <input type="number" name="stat" class="form-control" value="<?php echo $stat; ?>">
                <span class="help-block"><?php echo $stat_err; ?></span>
            </div>  
             <div class="form-group <?php echo (!empty($statType_err)) ? 'has-error' : ''; ?>">
                <label>Stat Type</label>
                <input type="text" name="statType" class="form-control" value="<?php echo $varTwo; ?>">
                <span class="help-block"><?php echo $statType_err; ?></span>
            </div>  
             <div class="form-group <?php echo (!empty($statID_err)) ? 'has-error' : ''; ?>">
                <label>Player ID</label>
                <input type="number" name="statID" class="form-control" value="<?php echo $var; ?>">
                <span class="help-block"><?php echo $statID_err; ?></span>
            </div>    
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit">
            </div>
            <p>This will update the weekly statistics table</a>.</p>
        </form>
    </div> 
    </div> 
</body>
</html>