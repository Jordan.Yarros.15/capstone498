<?php
        
/* Database credentials. Assuming you are running MySQL
server with default setting (user 'root' with no password) */
define('RDS_HOSTNAME', 'aa1u4nfojyef6ry.cpwluseulq90.us-west-2.rds.amazonaws.com');
define('RDS_USERNAME', 'JordanCapstone');
/* Secure password */
define('RDS_PASSWORD', '99109445Jy!');
define('RDS_DB_NAME', 'aa1u4nfojyef6ry');
define('RDS_Port', '3306');

/* Attempt to connect to MySQL database */
$link = mysqli_connect($_SERVER['RDS_HOSTNAME'], $_SERVER['RDS_USERNAME'], $_SERVER['RDS_PASSWORD'], $_SERVER['RDS_DB_NAME'], $_SERVER['RDS_PORT']);

// Check connection
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}
	
 $query = "SELECT id, firstName, lastName, position, salary, rating, team FROM players;";
 $result = mysqli_query($link, $query); 
 
 // Define variables and initialize with empty values
$addNewPlayerInfo_err = $firstName = $lastName = $team = $bio = $position = "";
$salary = $rating = 0;



// Close connection
mysqli_close($link);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<title>Premier Plaza Players</title>

	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.css">
    <!-- <link href="assets/css/bootstrap.css" rel="stylesheet" /> -->
    <link href="assets/css/fresh-bootstrap-table.css" rel="stylesheet" />
    <link href="assets/css/add_New_Player.css" rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
        
	
</head>
<body>

<div class="wrapper">
    <div class="fresh-table full-screen-table">
    <!--    Available colors for the full background: full-color-blue, full-color-azure, full-color-green, full-color-red, full-color-orange                  
            Available colors only for the toolbar: toolbar-color-blue, toolbar-color-azure, toolbar-color-green, toolbar-color-red, toolbar-color-orange
    -->
        
    
  			<div class="toolbar">
            	<a id="browseTitle" href="/welcome.php" class="btn btn-default">Home</a>
       		</div>

        
        
        <table id="fresh-table" class="table">
            <thead>
                <th data-field="id">ID</th>
            	<th data-field="fn" data-sortable="true">First Name</th>
            	<th data-field="ln" data-sortable="true">Last Name</th>
            	<th data-field="position" data-sortable="true">Position</th>
            	<th data-field="salary" data-sortable="true">Salary</th>
            	<th data-field="country" data-sortable="true">Rating</th>
            	<th data-field="city">Team</th>
            	<th data-field="actions" data-formatter="operateFormatter" data-events="operateEvents">Actions</th>
            </thead>
            <tbody>
          
         	<?php   
                          while($row = mysqli_fetch_array($result))  
                          {  
                               echo '  
                               <tr>  
                               		<td>'.$row["id"].'</td>  
                                    <td>'.$row["firstName"].'</td>  
                                    <td>'.$row["lastName"].'</td>
                                    <td>'.$row["position"].'</td>   
                                    <td>'.$row["salary"].'</td>  
                                    <td>'.$row["rating"].'</td>  
                                    <td>'.$row["team"].'</td>  
                               </tr>  
                               ';  
                          }  
            ?>  

           </tbody>
        </table> 
    </div>
    
</div>

<div class="modal fade login" id="playerBioModal">
		    <div class="modal-dialog login animated">
    		    <div class="modal-content">
    		         <div class="modal-header">
    		         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    		         <h4 class="modal-title">Add New Player</h4>
    		        		<div class="form addBioBox">
                                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                                    	<div class="form-group <?php echo (!empty($addNewPlayerInfo_err)) ? 'has-error' : ''; ?>">
                                    		<input id="playerFirstName" class="form-control" type="text" placeholder="First Name" name="playerFirstName">
                                    		<input id="playerLastName" class="form-control" type="text" placeholder="Last Name" name="playerLastName">
                                    		<input id="salary" class="form-control" type="text" placeholder="Salary" name="salary">
                                   			<input id="rating" class="form-control" type="text" placeholder="Rating" name="rating">
                                    		<input id="team" class="form-control" type="text" placeholder="Team" name="team">
                                    		<span class="help-block"><?php echo $addNewPlayerInfo_err; ?></span>
                                    	</div> 
                                    	<div class="form-group">
                                    		<input class="btn btn-default" type="submit" value="Submit"">
                                    	</div>
                                    </form>
                                </div>
    		         </div>
    		    </div>
    		</div>
    	</div>

</body>
    <script type="text/javascript" src="assets/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap-table.js"></script>
        
    <script type="text/javascript">
        var $table = $('#fresh-table'),
            $Bio = $('#Bio'), 
            full_screen = false,
            window_height;
            
        $().ready(function(){
            
            window_height = $(window).height();
            table_height = window_height - 20;
            
            
            $table.bootstrapTable({
                toolbar: ".toolbar",

                showRefresh: true,
                search: true,
                showToggle: true,
                showColumns: true,
                pagination: true,
                striped: true,
                sortable: true,
                height: table_height,
                pageSize: 25,
                pageList: [25,50,100],
                
                formatShowingRows: function(pageFrom, pageTo, totalRows){
                    //do nothing here, we don't want to show the text "showing x of y from..." 
                },
                formatRecordsPerPage: function(pageNumber){
                    return pageNumber + " rows visible";
                },
                icons: {
                    refresh: 'fa fa-refresh',
                    toggle: 'fa fa-th-list',
                    columns: 'fa fa-columns',
                    detailOpen: 'fa fa-plus-circle',
                    detailClose: 'fa fa-minus-circle'
                }
            });
            
            window.operateEvents = {
                'click .like': function (e, value, row, index) {
                    alert('You click like icon, row: ' + JSON.stringify(row));
                    console.log(value, row, index);
                },
                'click .edit': function (e, value, row, index) {
                    alert('You click edit icon, row: ' + JSON.stringify(row));
                    console.log(value, row, index);    
                },
                'click .remove': function (e, value, row, index) {
                    $table.bootstrapTable('remove', {
                        field: 'id',
                        values: [row.id]
                    });
            
                }
            };
            
                function bioForm(){
    	$('#playerBioModal .addBioBox').fadeOut('fast',function(){
        $('.addBioBox').fadeIn('fast');
        
        
       
    });       
     $('.error').removeClass('alert alert-danger').html(''); 
}

function bioModal(){
    bioForm();
    setTimeout(function(){
        $('#playerBioModal').modal('show');    
    }, 230);
    
}

            $Bio.click(function () {
                bioModal();
            });
        
            
            $(window).resize(function () {
                $table.bootstrapTable('resetView');
            });    
        });

        function operateFormatter(value, row, index) {
            return [
                '<a rel="tooltip" title="Like" class="table-action like" href="javascript:void(0)" title="Like">',
                    '<i class="fa fa-heart"></i>',
                '</a>',
                '<a rel="tooltip" title="Edit" class="table-action edit" href="javascript:void(0)" title="Edit">',
                    '<i class="fa fa-edit"></i>',
                '</a>',
                '<a rel="tooltip" title="Bio" title="Bio">',
                    '<i class="fa fa-user"  href="javascript:void(0)"></i>',
                '</a>',
                '<a rel="tooltip" title="Stats"  href="http://premierplaza-env-1.2qvmgmhmmu.us-west-2.elasticbeanstalk.com/x_fresh_bootstrap_table_v1.1/stats.php" title="Stats">',
                    '<i class="fa fa-bar-chart" aria-hidden="true"></i>',
                '</a>',
                '<a rel="tooltip" title="Remove" class="table-action remove" href="javascript:void(0)" title="Remove">',
                    '<i class="fa fa-remove"></i>',
                '</a>'
            ].join('');
        }
       
    </script>

</html>