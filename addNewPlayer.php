<?php

// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Check if the user id is assigned
if(!isset($_SESSION["id"])){
    header("location: login.php");
    exit;
} else {
	$sessionUsername = trim($_SESSION["username"]);
}

/* Database credentials. Assuming you are running MySQL
server with default setting (user 'root' with no password) */
define('RDS_HOSTNAME', 'aa1u4nfojyef6ry.cpwluseulq90.us-west-2.rds.amazonaws.com');
define('RDS_USERNAME', 'JordanCapstone');
/* Secure password */
define('RDS_PASSWORD', '99109445Jy!');
define('RDS_DB_NAME', 'aa1u4nfojyef6ry');
define('RDS_Port', '3306');

/* Attempt to connect to MySQL database */
$link = mysqli_connect($_SERVER['RDS_HOSTNAME'], $_SERVER['RDS_USERNAME'], $_SERVER['RDS_PASSWORD'], $_SERVER['RDS_DB_NAME'], $_SERVER['RDS_PORT']);

// Check connection
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}

// Define variables and initialize with empty values
$firstName = $lastName = $position = $team = "";
$salary = $rating = 0;
$firstName_err = $lastName_err = $position_err = $salary_err = $rating_err = $team_err = "";

// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){

    // Validate firstName
    if(empty(trim($_POST["firstName"]))){
        $firstName_err = "Please enter a First name.";
    } elseif(empty(trim($_POST["lastName"]))){
        $lastName_err = "Please enter a Last name.";
    } elseif(empty(trim($_POST["position"]))){
        $position_err = "Please enter a position.";
    } elseif(empty(trim($_POST["salary"]))){
        $salary_err = "Please enter a salary.";
    } elseif(empty(trim($_POST["rating"]))){
        $rating_err = "Please enter a rating.";
    } elseif(empty(trim($_POST["team"]))){
        $team_err = "Please enter a team name.";
    } else{
        // Prepare a select statement
        $sql = "SELECT id FROM players WHERE firstName = ? AND lastName = ?";

        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "ss", $param_firstName, $param_lastName);

            // Set parameters
           	$param_firstName = trim($_POST["firstName"]);
			$param_lastName = trim($_POST["lastName"]);
			
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                /* store result */
                mysqli_stmt_store_result($stmt);

                if(mysqli_stmt_num_rows($stmt) == 1){
                	$firstName_err = "This first and last name are already taken.";
                    $lastName_err = "This first and last name are already taken.";
                } else{
                    $firstName = trim($_POST["firstName"]);
                    $lastName = trim($_POST["lastName"]);
                    $position = trim($_POST["position"]);
                    $salary = trim($_POST["salary"]);
                    $rating = trim($_POST["rating"]);
                    $team = trim($_POST["team"]);
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }

        // Close statement
        mysqli_stmt_close($stmt);
    }

    // Check input errors before inserting in database
    if(empty($firstName_err) && empty($lastName_err) && empty($position_err)  && empty($salary_err)  && empty($rating_err)  && empty($team_err)){

        // Prepare an insert statement
        $sql = "insert into players (firstName, lastName, position, salary, rating, team) VALUES (?, ?, ?, ?, ?, ?);";

        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "sssiis", $param_firstName, $param_lastName, $param_position,$param_salary , $param_rating ,$param_team);

            // Set parameters
            $param_firstName = $firstName;
            $param_lastName = $lastName; 
            $param_position = $position;
            $param_salary = $salary; 
            $param_rating = $rating;
            $param_team = $team; 

            // Attempt to execute the prepared statement
            if (mysqli_stmt_execute($stmt)){
            
             		
             $insert = "insert into playersWeeklyStats (goals, assists, shots,shotsOT, touches, passes, throughBalls, crosses, corners, blocks,interceptions,tackles,clearances, arialBattlesWon,cleanSheets,saves) VALUES (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);";
             $insertStmt = mysqli_prepare($link, $insert);
 			 mysqli_stmt_execute($insertStmt);
 	
 					
            
                // Redirect to login page
                header("location: welcome.php");
            } else{
                echo "Something went wrong. Please try again later.";
            }
        }

        // Close statement
        mysqli_stmt_close($stmt);
    }

    // Close connection
    mysqli_close($link);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sign Up</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        .wrap{
        font: 14px sans-serif; 
        display: -webkit-box;
  		display: flex;
  		-ms-flex-align: center;
  		-ms-flex-pack: center;
  		-webkit-box-align: center;
  		align-items: center;
  		-webkit-box-pack: center;
  		justify-content: center;
  		padding-top: 40px;
  		padding-bottom: 40px;
  		background-color: #f5f5f5;}
        .wrapper{ width: 350px; padding: 20px; }
        .navbar{padding-right: 20px;}
    </style>
</head>
 


<body>
   
    <nav class="navbar navbar-expand-lg navbar-default navbar-inverse bg-light">
    <ul class="nav navbar-nav">
      <li>
        <a class="nav-link" href="joinLeague.php">Join League </a>
      </li>
      <li>
      <a class="nav-link" href="createLeague.php">Create League </a>
      </li>
      <li>
        <a class="nav-link" href="/x_fresh_bootstrap_table_v1.1/full-screen-table.php">Browse Players</a>
      </li>
       <li>
        <a class="nav-link" href="addNewPlayer.php">Add New Player</a>
      </li>
      <li>
        <a class="nav-link" href="/x_fresh_bootstrap_table_v1.1/stats.php">Game Day Stats</a>
      </li>
      </ul>
    <span>
    <ul class="nav navbar-nav navbar-right">
      <li>
        <a class="nav-link" href="reset-password.php">Reset Password</a>
      </li>
      <li>
     	<a class="nav-link" href="logout.php">Logout</a>
      </li>
    </ul>
    </span>
  </div>
</nav>

<div class = wrap>
     <div class="wrapper">
        <h2>Add a new player</h2>
        <p>Please fill this form to create a new player. <?php echo $param_ID ?></p>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group <?php echo (!empty($firstName_err)) ? 'has-error' : ''; ?>">
                <label>First Name</label>
                <input type="text" name="firstName" class="form-control" value="<?php echo $firstName; ?>">
                <span class="help-block"><?php echo $firstName_err; ?></span>
            </div>
            <div class="form-group <?php echo (!empty($lastName_err)) ? 'has-error' : ''; ?>">
                <label>Last Name</label>
                <input type="text" name="lastName" class="form-control" value="<?php echo $lastName; ?>">
                <span class="help-block"><?php echo $lastName_err; ?></span>
            </div>
            <div class="form-group <?php echo (!empty($position_err)) ? 'has-error' : ''; ?>">
                <label>Position</label>
                <select class="w3-select" name="position">
               	 	<option value="" disabled selected>Choose Player Position</option>
  					<option value="Forward">Forward</option>
  					<option value="Defender">Defender</option>
  					<option value="Midfield">Midfield</option>
                </select>
                <span class="help-block"><?php echo $position_err; ?></span>
            </div>
             <div class="form-group <?php echo (!empty($salary_err)) ? 'has-error' : ''; ?>">
                <label>Salary</label>
                <input type="number" name="salary" class="form-control" value="<?php echo $salary; ?>">
                <span class="help-block"><?php echo $salary_err; ?></span>
            </div>
             <div class="form-group <?php echo (!empty($rating_err)) ? 'has-error' : ''; ?>">
                <label>Rating</label>
                <input type="number" name="rating" class="form-control" value="<?php echo $rating; ?>">
                <span class="help-block"><?php echo $rating_err; ?></span>
            </div>
             <div class="form-group <?php echo (!empty($team_err)) ? 'has-error' : ''; ?>">
                <label>Team</label>
                <input type="text" name="team" class="form-control" value="<?php echo $team; ?>">
                <span class="help-block"><?php echo $team_err; ?></span>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit">
                <input type="reset" class="btn btn-default" value="Reset">
            </div>
            <p>Please make sure all fields are filled.</p>
        </form>
    </div>
    </div>
</body>
</html>
