<?php

// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Check if the user id is assigned
if(!isset($_SESSION["id"])){
    header("location: login.php");
    exit;
} else {
	$sessionUsername = trim($_SESSION["username"]);
	$sessionID = trim($_SESSION["id"]);
}

// Include config file
require_once "config.php";

// Define variables and initialize with empty values
$leagueName = $password = $confirm_password = "";
$username_err = $password_err = $confirm_password_err = "";

// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){

    // Validate leagueName
    if(empty(trim($_POST["leagueName"]))){
        $username_err = "Please enter a league name.";
    } else{
        // Prepare a select statement
        $sql = "SELECT id FROM leagues WHERE leagueName = ?";

        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_username);

            // Set parameters
            $param_username = trim($_POST["leagueName"]);

            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                /* store result */
                mysqli_stmt_store_result($stmt);

                if(mysqli_stmt_num_rows($stmt) == 1){
                    $username_err = "This league name is already taken.";
                } else{
                    $leagueName = trim($_POST["leagueName"]);
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }

        // Close statement
        mysqli_stmt_close($stmt);
    }

    // Validate password
    if(empty(trim($_POST["password"]))){
        $password_err = "Please enter a password.";
    } elseif(strlen(trim($_POST["password"])) < 6){
        $password_err = "Password must have atleast 6 characters.";
    } else{
        $password = trim($_POST["password"]);
    }

    // Validate confirm password
    if(empty(trim($_POST["confirm_password"]))){
        $confirm_password_err = "Please confirm password.";
    } else{
        $confirm_password = trim($_POST["confirm_password"]);
        if(empty($password_err) && ($password != $confirm_password)){
            $confirm_password_err = "Password did not match.";
        }
    }

    // Check input errors before inserting in database
    if(empty($username_err) && empty($password_err) && empty($confirm_password_err)){

        // Prepare an insert statement
        $sql = "
        insert into leagues (leagueName, userOne, password) VALUES (?, ?, ?);";

        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "sss", $param_username, $sessionUsername, $param_password);


            // Set parameters
            $param_username = $leagueName;
            $param_password = $password; // Creates a password hash
			$hasTheTeamBeenPicked = False;
			
			
			
					 $query = "insert into leagueTeam (userID, leagueName, hasTheTeamBeenPicked) VALUES (?, ?, ?);";

        				if($stmtTwo = mysqli_prepare($link, $query)){
            				// Bind variables to the prepared statement as parameters
            				mysqli_stmt_bind_param($stmtTwo, "sss", $sessionID, $param_username, $hasTheTeamBeenPicked);
							
							 // Attempt to execute the prepared statement
           					 if(mysqli_stmt_execute($stmtTwo)){
           					 
           					 $insert = "insert into leaguePoints (userID,leagueName, userTotalPoints, userWeeklyPoints) VALUES (".$sessionID.",'".$leagueName."', 0,0);";
             				$insertStmt = mysqli_prepare($link, $insert);
 			 				mysqli_stmt_execute($insertStmt);
               				 
            				} else{
                				echo "Something went wrong. Please try again later.";
            				}
						}
						

						
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
            
            $sql2 = "update leagues set expiresOn = date_add(createdOn, interval 7 day) where id = ".$sessionID.";";
			if($stmtThree = mysqli_prepare($link, $sql2)){
				// Attempt to execute the prepared statement
           					 if(mysqli_stmt_execute($stmtThree)){
               				 
            				} else{
                				echo "Something went wrong. Please try again later.";
            				}
			}
                // Redirect to login page
                header("location: welcome.php");
            } else{
                echo "Something went wrong. Please try again later.";
            }
        }

        // Close statement
        mysqli_stmt_close($stmt);
        mysqli_stmt_close($stmtTwo);
    }

    // Close connection
    mysqli_close($link);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sign Up</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        .wrap{
        font: 14px sans-serif; 
        display: -webkit-box;
  		display: flex;
  		-ms-flex-align: center;
  		-ms-flex-pack: center;
  		-webkit-box-align: center;
  		align-items: center;
  		-webkit-box-pack: center;
  		justify-content: center;
  		padding-top: 40px;
  		padding-right: 10px;
  		padding-bottom: 40px;
  		background-color: #f5f5f5;}
        .wrapper{ width: 350px; padding: 20px; }
        .navbar{padding-right: 20px;}
    </style>
</head>
 
<body>
   
    <nav class="navbar navbar-expand-lg navbar-default navbar-inverse bg-light">
    <ul class="nav navbar-nav">
    <li>
        <a class="nav-link" href="welcome.php">Home </a>
      </li>
      <li>
        <a class="nav-link" href="joinLeague.php">Join League </a>
      </li>
      <li>
      <a class="nav-link" href="createLeague.php">Create League </a>
      </li>
      <li>
        <a class="nav-link" href="/x_fresh_bootstrap_table_v1.1/full-screen-table.php">Browse Players</a>
      </li>
       <li>
        <a class="nav-link" href="addNewPlayer.php">Add New Player</a>
      </li>
      <li>
        <a class="nav-link" href="/x_fresh_bootstrap_table_v1.1/stats.php">Game Day Stats</a>
      </li>
      </ul>
    <span>
    <ul class="nav navbar-nav navbar-right">
      <li >
        <a class="nav-link" href="reset-password.php">Reset Password</a>
      </li>
      <li>
     	<a class="nav-link" href="logout.php">Logout</a>
      </li>
    </ul>
    </span>
</nav>

<div class = wrap>
     <div class="wrapper">
        <h2>Create a League</h2>
        <p>Please fill this form to create a League.</p>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
                <label>League Name</label>
                <input type="text" name="leagueName" class="form-control" value="<?php echo $leagueName; ?>">
                <span class="help-block"><?php echo $username_err; ?></span>
            </div>
            <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                <label>Password</label>
                <input type="password" name="password" class="form-control" value="<?php echo $password; ?>">
                <span class="help-block"><?php echo $password_err; ?></span>
            </div>
            <div class="form-group <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">
                <label>Confirm Password</label>
                <input type="password" name="confirm_password" class="form-control" value="<?php echo $confirm_password; ?>">
                <span class="help-block"><?php echo $confirm_password_err; ?></span>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit">
                <input type="reset" class="btn btn-default" value="Reset">
            </div>
            <p>The last time you can Join a league is seven days after creation. Please send your friends the password and 
            league name so they can join. There are at most twelve users per league</p>
        </form>
    </div>
    </div>
</body>
</html>
