<?php
// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Check if the user id is assigned
if(!isset($_SESSION["id"])){
    header("location: login.php");
    exit;
} else {
	$sessionUsername = trim($_SESSION["username"]);
	$sessionID = trim($_SESSION["id"]);
}
 
// Include config file
require_once "config.php";
 
// Define variables and initialize with empty values
$leagueName = $password = "";
$username_err = $password_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    // Check if leagueName is empty
    if(empty(trim($_POST["leagueName"]))){
        $username_err = "Please enter league Name.";
    } else{
        $leagueName = trim($_POST["leagueName"]);
    }
    
    // Check if password is empty
    if(empty(trim($_POST["password"]))){
        $password_err = "Please enter your password.";
    } else{
        $password = trim($_POST["password"]);
    }
    
    // Validate credentials
    if(empty($username_err) && empty($password_err)){
        // Prepare a select statement
        $sql = "SELECT * FROM leagues WHERE leagueName = ?";
        
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            
            // Set parameters
            $param_username = $leagueName;
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Store result
                mysqli_stmt_store_result($stmt);
                
                // Check if leagueName exists exists, if yes then verify password
                if(mysqli_stmt_num_rows($stmt) == 1){                    
                    // Bind result variables
                    mysqli_stmt_bind_result($stmt, $id, $actualLeagueName, $db_password, $userOne, $userTwo, $userThree, $userFour, $userFive, $userSix, 
        		$userSeven, $userEight, $userNine, $userTen, $userEleven, $userTwelve, $createdOn, $expiresOn, $isLeagueOrderSet);
                    
                    //if ($createdOn < $expiresOn){
                    if(mysqli_stmt_fetch($stmt)){
                        if($password == $db_password){
                                                  
                            if (empty($userTwo)) { $addUser = 'userTwo';} 
                            elseif (empty($userThree)) { $addUser = 'userThree';}   
                            elseif (empty($userFour)) { $addUser = 'userFour';}   
                            elseif (empty($userFive)) { $addUser = 'userFive';}   
                            elseif (empty($userSix)) { $addUser = 'userSix';}   
                            elseif (empty($userSeven)) { $addUser = 'userSeven';}   
                            elseif (empty($userEight)) { $addUser = 'userEight';}  
                            elseif (empty($userNine)) { $addUser = 'userNine';}   
                            elseif (empty($userTen)) { $addUser = 'userTen';}   
                            elseif (empty($userEleven)) { $addUser = 'userEleven';}  
                            elseif (empty($userTwelve)) { $addUser = 'userTwelve';}     
                            
                            // write username into leagues database in an open column
                            // if mktime(0, 0, 0, date("m")  , date("d")+1, date("Y")); = lastDayToJoin then no
                             // Prepare an insert statement
                             $insert = "insert into leaguePoints (userID,leagueName, userTotalPoints, userWeeklyPoints) VALUES (".$sessionID.",'".$leagueName."', 0,0);";
             				$insertStmt = mysqli_prepare($link, $insert);
 			 				mysqli_stmt_execute($insertStmt);
                             
       						 $sql = "update leagues set ".$addUser." = ? where leagueName = '".$leagueName."';";
       						  	if($stmt = mysqli_prepare($link, $sql)){
            				// Bind variables to the prepared statement as parameters
            				mysqli_stmt_bind_param($stmt, "s", $sessionUsername);

            				// Attempt to execute the prepared statement
            				if(mysqli_stmt_execute($stmt)){
                				// Redirect to login page
                				header("location: welcome.php");
           			 } else{
              				  echo "Something went wrong. Please try again later.";
           			 }
        		}

       					 // Close statement
        				mysqli_stmt_close($stmt);
                            
                        } else{
                            // Display an error message if password is not valid
                            $password_err = "The password you entered was not valid.";
                        }
                    }
                } else{
                    // Display an error message if username doesn't exist
                    $username_err = "No account found with that username.";
                }
                //}
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }
        
        // Close statement
        mysqli_stmt_close($stmt);
    }
    
    // Close connection
    mysqli_close($link);
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Join League</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        .wrap{ 
        font: 14px sans-serif; 
        display: -webkit-box;
  		display: flex;
  		-ms-flex-align: center;
  		-ms-flex-pack: center;
  		-webkit-box-align: center;
  		align-items: center;
  		-webkit-box-pack: center;
  		justify-content: center;
  		padding-top: 40px;
  		padding-bottom: 40px;
  		background-color: #f5f5f5;
        }
        .wrapper{ width: 350px; padding: 20px; }
        .navbar{padding-right: 20px;}
    </style>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-default navbar-inverse bg-light">
    <ul class="nav navbar-nav">
    <li>
        <a class="nav-link" href="welcome.php">Home </a>
      </li>
      <li>
        <a class="nav-link" href="joinLeague.php">Join League </a>
      </li>
      <li>
      <a class="nav-link" href="createLeague.php">Create League </a>
      </li>
      <li>
        <a class="nav-link" href="/x_fresh_bootstrap_table_v1.1/full-screen-table.php">Browse Players</a>
      </li>
       <li>
        <a class="nav-link" href="addNewPlayer.php">Add New Player</a>
      </li>
      <li>
        <a class="nav-link" href="/x_fresh_bootstrap_table_v1.1/stats.php">Game Day Stats</a>
      </li>
      </ul>
    <span>
    <ul class="nav navbar-nav navbar-right">
      <li>
        <a class="nav-link" href="reset-password.php">Reset Password</a>
      </li>
      <li>
     	<a class="nav-link" href="logout.php">Logout</a>
      </li>
    </ul>
    </span>
  </div>
</nav>
    </div>
    <div class = wrap>
     <div class="wrapper">
        <h2>Join League</h2>
        <p>Please fill in the credentials to join a league.</p>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
                <label>League Name</label>
                <input type="text" name="leagueName" class="form-control" value="<?php echo $leagueName; ?>">
                <span class="help-block"><?php echo $username_err; ?></span>
            </div>    
            <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                <label>League Password</label>
                <input type="password" name="password" class="form-control">
                <span class="help-block"><?php echo $password_err; ?></span>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Join">
            </div>
        </form>
    </div> 
    </div>   
</body>
</html>